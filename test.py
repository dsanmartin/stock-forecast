import numpy as np # Numerical Python
import pandas as pd # Statistics 
import matplotlib.pyplot as plt # Plots
from sklearn.preprocessing import MinMaxScaler, StandardScaler # Scalers
from sklearn.model_selection import train_test_split # Train/Test
from sklearn.metrics import mean_absolute_error, mean_squared_error 
# ANN API
from keras.models import Sequential
from keras.layers import Dense
from keras import optimizers

#%% Create input target for time serie
def slidingWindow(data, lags=3):
    samples = len(data) - lags
    X = np.zeros((samples, lags))
    y = np.zeros(samples)
    for i in range(samples):
        X[i] = data[i:lags+i]
        y[i] = data[i+lags]
    return X, y

# Plot serie
def plotData(data, title='Serie'):
    plt.title(title)
    plt.plot(data)
    plt.grid(True)
    plt.show()
    
#%% 
data = pd.read_csv('data/ipsa_201801-201903.csv', index_col=False, header=0) # Read file
serie = data.iloc[:,1] # Only 'Last' column
serie = np.array( # Transform to numpy float
        map(float, 
            map(lambda x: x.replace(',', '.'), 
                map(lambda x: x.replace('.', ''), serie))))
# Plot serie
plotData(serie)

#%% Create train/test input-target
# With lag=3 predict the next value, f(x_{t-1}, x_{t_2}, x_{t-3}) predict x_{t}
X, y = slidingWindow(serie, 2)

# Split train/test
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, shuffle=False)

#%%
# Scalers for data
y_train = y_train.reshape(len(y_train), 1) # Scaler uses this data shape for unidimensional data
std_scaler_x = StandardScaler().fit(X_train) # Standard scaler
mm_scaler_x = MinMaxScaler(feature_range=(0, 1)).fit(X_train) # MinMax scaler
std_scaler_y = StandardScaler().fit(y_train) # Standard scaler
mm_scaler_y = MinMaxScaler(feature_range=(0, 1)).fit(y_train) # MinMax scaler
Xtr_std = std_scaler_x.transform(X_train)
Xtr_mm = mm_scaler_x.transform(X_train)
ytr_std = std_scaler_y.transform(y_train)
ytr_mm = mm_scaler_y.transform(y_train)
# Only standard scale will be tested...

#%% ANN architecture
# 3      32   16      1
# Input - Hiddens - Output
model = Sequential()
model.add(Dense(units=32, activation='relu', input_dim=Xtr_std.shape[1])) # Hidden layer
model.add(Dense(units=16, activation='relu')) # Hidden layer
model.add(Dense(units=1, activation='tanh')) # Output
# Define loss function, optimizer, metrics...
sgd = optimizers.SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
model.compile(loss='mean_squared_error', optimizer=sgd, metrics=['mae', 'mse'])

#%% Train model
epochs_ = 1000
batch_size_ = 32
hist = model.fit(Xtr_std, ytr_std, epochs=epochs_, batch_size=batch_size_)

#%% Training plot
plt.figure(figsize=(10, 4))
plt.subplot(1, 2, 1)
plt.title('MSE vs Epoch')
plt.plot(hist.history['mean_squared_error'])
plt.grid(True)
plt.subplot(1, 2, 2)
plt.title('MAE vs Epoch')
plt.plot(hist.history['mean_absolute_error'])
plt.tight_layout()
plt.grid(True)
plt.show()

#%% Predict and evaluate
Xte = std_scaler_x.transform(X_test)
yte = std_scaler_y.transform(y_test.reshape(len(y_test), 1))
predict = model.predict(Xte)

# Testing metrics
predicted_it = std_scaler_y.inverse_transform(predict)
print("MSE: ", mean_squared_error(y_test, predicted_it))
print("MAE: ", mean_absolute_error(y_test, predicted_it))

#%% Plot predict and real
plt.plot(y_test, 'b-o', label="Real")
plt.plot(predicted_it, 'r-x', label="Predicted")
plt.grid(True)
plt.legend()
plt.show()